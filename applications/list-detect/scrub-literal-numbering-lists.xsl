<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsw="http://coko.foundation/xsweet"
  xmlns="http://www.w3.org/1999/xhtml"
  xpath-default-namespace="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="#all">

  <xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
  <xsl:mode on-no-match="shallow-copy"/>

<!-- find all li:p s and scrub the leading numeral(s) + . + ws -->
  <xsl:template match="li/p[1]">
    <xsl:value-of select='replace(., "^\s*\d+\.\s+", "")'/>
  </xsl:template>

</xsl:stylesheet>
 