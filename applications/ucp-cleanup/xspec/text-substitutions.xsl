<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:test="http://www.jenitennison.com/xslt/unit-test"
                xmlns:x="http://www.jenitennison.com/xslt/xspec"
                xmlns:__x="http://www.w3.org/1999/XSL/TransformAliasAlias"
                xmlns:pkg="http://expath.org/ns/pkg"
                xmlns:impl="urn:x-xspec:compile:xslt:impl"
                xmlns="http://www.w3.org/1999/xhtml"
                version="2"
                exclude-result-prefixes="pkg impl">
   <xsl:import href="file:/C:/Users/Wendell/Documents/Gitlab/HTMLevator/applications/ucp-cleanup/ucp-text-macros.xsl"/>
   <xsl:import href="file:/C:/Users/Wendell/AppData/Roaming/com.oxygenxml/extensions/v20.1/frameworks/https___raw.githubusercontent.com_xspec_oXygen_XML_editor_xspec_support_master_build_update_site.xml/xspec.support-1.2.1/src/compiler/generate-tests-utils.xsl"/>
   <xsl:import href="file:/C:/Users/Wendell/AppData/Roaming/com.oxygenxml/extensions/v20.1/frameworks/https___raw.githubusercontent.com_xspec_oXygen_XML_editor_xspec_support_master_build_update_site.xml/xspec.support-1.2.1/src/schematron/sch-location-compare.xsl"/>
   <xsl:namespace-alias stylesheet-prefix="__x" result-prefix="xsl"/>
   <xsl:variable name="x:stylesheet-uri"
                 as="xs:string"
                 select="'file:/C:/Users/Wendell/Documents/Gitlab/HTMLevator/applications/ucp-cleanup/ucp-text-macros.xsl'"/>
   <xsl:output name="x:report" method="xml" indent="yes"/>
   <xsl:template name="x:main">
      <xsl:message>
         <xsl:text>Testing with </xsl:text>
         <xsl:value-of select="system-property('xsl:product-name')"/>
         <xsl:text> </xsl:text>
         <xsl:value-of select="system-property('xsl:product-version')"/>
      </xsl:message>
      <xsl:result-document format="x:report">
         <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="file:/C:/Users/Wendell/AppData/Roaming/com.oxygenxml/extensions/v20.1/frameworks/https___raw.githubusercontent.com_xspec_oXygen_XML_editor_xspec_support_master_build_update_site.xml/xspec.support-1.2.1/src/compiler/format-xspec-report.xsl"</xsl:processing-instruction>
         <x:report stylesheet="{$x:stylesheet-uri}"
                   date="{current-dateTime()}"
                   xspec="file:/C:/Users/Wendell/Documents/Gitlab/HTMLevator/applications/ucp-cleanup/text-substitutions.xspec">
            <xsl:call-template name="x:xbaa809d2-a24e-3d42-9025-37acd773d060"/>
            <xsl:call-template name="x:x15bc6d43-b57d-327b-98d5-e176d33af497"/>
         </x:report>
      </xsl:result-document>
   </xsl:template>
   <xsl:template name="x:xbaa809d2-a24e-3d42-9025-37acd773d060">
      <xsl:message>a</xsl:message>
      <x:scenario source="file:/C:/Users/Wendell/Documents/Gitlab/HTMLevator/applications/ucp-cleanup/text-substitutions.xspec"
                  template-id="xbaa809d2-a24e-3d42-9025-37acd773d060">
         <x:label>a</x:label>
         <x:context>
            <body>
               <p>
                  <xsl:text>baby </xsl:text>
                  <b>
                     <xsl:text>bold</xsl:text>
                  </b>
               </p>
               <p>
                  <xsl:text>"'quote'"</xsl:text>
               </p>
            </body>
         </x:context>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc" as="document-node()">
               <xsl:document>
                  <body>
                     <p>
                        <xsl:text>baby </xsl:text>
                        <b>
                           <xsl:text>bold</xsl:text>
                        </b>
                     </p>
                     <p>
                        <xsl:text>"'quote'"</xsl:text>
                     </p>
                  </body>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="impl:context" select="$impl:context-doc/node()"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d7e11">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d7e11">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>quotes substituted including hair spaces char 200A</xsl:message>
      <xsl:variable name="impl:expected-doc" as="document-node()">
         <xsl:document>
            <body>
               <p>
                  <xsl:text>baby </xsl:text>
                  <b>
                     <xsl:text>bold</xsl:text>
                  </b>
               </p>
               <p>
                  <xsl:text>&#x201c;&#x200a;&#x2018;quote&#x2019;&#x200a;&#x201d;</xsl:text>
               </p>
            </body>
         </xsl:document>
      </xsl:variable>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/node()"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>quotes substituted including hair spaces char 200A</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:x15bc6d43-b57d-327b-98d5-e176d33af497">
      <xsl:message>Big Grab Bag</xsl:message>
      <x:scenario source="file:/C:/Users/Wendell/Documents/Gitlab/HTMLevator/applications/ucp-cleanup/text-substitutions.xspec"
                  template-id="x15bc6d43-b57d-327b-98d5-e176d33af497">
         <x:label>Big Grab Bag</x:label>
         <x:context>
            <html>
               <head>
                  <meta charset="UTF-8"/>
               </head>
               <body>
                  <div class="docx-body">
                     <p>
                        <span class="tab">
                           <xsl:comment> tab </xsl:comment>
                        </span>
                     </p>
                     <p>
                        <xsl:text>Paragraph above is just a tab</xsl:text>
                     </p>
                     <p>
                        <xsl:text>"'quote'"</xsl:text>
                     </p>
                     <p>
                        <xsl:text>'"quote"'</xsl:text>
                     </p>
                     <p>
                        <xsl:text>'&#x201d;quote"&#x2018;</xsl:text>
                     </p>
                     <p>
                        <xsl:text>&#x2018;"quote"&#x2019;</xsl:text>
                     </p>
                     <p>
                        <xsl:text>""quote""</xsl:text>
                     </p>
                     <p>
                        <xsl:text>&#x2014;&#x201d;</xsl:text>
                     </p>
                     <p>
                        <xsl:text>&#x201c;&#x2014;</xsl:text>
                     </p>
                     <p>
                        <xsl:text>&#x2019; quotation &#x2019;</xsl:text>
                     </p>
                     <p>
                        <xsl:comment> empty </xsl:comment>
                     </p>
                     <h2 style="font-family: Helvetica; font-weight: bold">
                        <xsl:text>These work:</xsl:text>
                     </h2>
                     <p>
                        <xsl:text>"straight quotes"</xsl:text>
                     </p>
                     <p>
                        <xsl:text>straight apostrophe'</xsl:text>
                     </p>
                     <p>
                        <xsl:text>don't</xsl:text>
                     </p>
                     <p>
                        <xsl:text>don&#x2018;t</xsl:text>
                     </p>
                     <p>
                        <xsl:text>don&#x2019;t</xsl:text>
                     </p>
                     <p>
                        <xsl:text>'simple'</xsl:text>
                     </p>
                     <p>
                        <xsl:text>`quote`</xsl:text>
                     </p>
                     <p>
                        <xsl:text>``quote``</xsl:text>
                     </p>
                     <p>
                        <xsl:text>&#x2018;qoutation&#x2018;</xsl:text>
                     </p>
                     <p>
                        <xsl:text>&#x201c;quotation&#x201c;</xsl:text>
                     </p>
                     <p>
                        <xsl:text>&#x2019;quotation&#x2019;</xsl:text>
                     </p>
                     <p>
                        <xsl:text>&#x201d;quotation&#x201d;</xsl:text>
                     </p>
                     <p>
                        <xsl:text>&#x201d;quotation``</xsl:text>
                     </p>
                     <p>
                        <xsl:text>``quotation&#x201d;</xsl:text>
                     </p>
                     <p>
                        <xsl:comment> empty </xsl:comment>
                     </p>
                     <p>
                        <xsl:text>sentence with punctuations separated by space .</xsl:text>
                     </p>
                     <p>
                        <xsl:text>sentence with punctuations separated by space !</xsl:text>
                     </p>
                     <p>
                        <xsl:text>sentence with punctuations separated by space ,</xsl:text>
                     </p>
                     <p>
                        <xsl:text>sentence with punctuations separated by space ,</xsl:text>
                     </p>
                     <p>
                        <xsl:text>sentence with punctuations separated by space ;</xsl:text>
                     </p>
                     <p>
                        <xsl:text>sentence with punctuations separated by space ?</xsl:text>
                     </p>
                     <p>
                        <xsl:text>sentence with punctuations separated by 3 spaces   ?</xsl:text>
                     </p>
                     <p>
                        <xsl:comment> empty </xsl:comment>
                     </p>
                     <p>
                        <xsl:comment> empty </xsl:comment>
                     </p>
                     <p>
                        <b>
                           <xsl:text>this is all bold except for the period</xsl:text>
                        </b>
                        <xsl:text>.</xsl:text>
                     </p>
                     <p>
                        <i>
                           <xsl:text>this is all italics except for the period</xsl:text>
                        </i>
                        <xsl:text>.</xsl:text>
                     </p>
                     <p>
                        <span style="font-family: Helvetica">
                           <u>
                              <xsl:text>this is all underlined</xsl:text>
                           </u>
                        </span>
                        <a class="bookmarkStart" id="docx-bookmark_0">
                           <xsl:comment> bookmark ='_GoBack'</xsl:comment>
                        </a>
                        <a href="#docx-bookmark_0">
                           <xsl:comment> bookmark end </xsl:comment>
                        </a>
                        <span style="font-family: Helvetica">
                           <u>
                              <xsl:text> except for the period</xsl:text>
                           </u>
                           <xsl:text>.</xsl:text>
                        </span>
                     </p>
                     <p>
                        <xsl:comment> empty </xsl:comment>
                     </p>
                     <p>
                        <xsl:text>Summer of '69</xsl:text>
                     </p>
                     <p>
                        <xsl:text>Summer of &#x2018;69</xsl:text>
                     </p>
                     <p>
                        <xsl:comment> empty </xsl:comment>
                     </p>
                     <p>
                        <xsl:text>W.E.B. Dubois</xsl:text>
                     </p>
                     <p>
                        <xsl:text>E. B. White</xsl:text>
                     </p>
                     <p>
                        <xsl:comment> empty </xsl:comment>
                     </p>
                     <p>
                        <xsl:text>U.S.</xsl:text>
                     </p>
                     <p>
                        <xsl:text>D.C.</xsl:text>
                     </p>
                     <p>
                        <xsl:text>A.M.</xsl:text>
                     </p>
                     <p>
                        <xsl:text>P.M.</xsl:text>
                     </p>
                     <p>
                        <xsl:text>A.D.</xsl:text>
                     </p>
                     <p>
                        <xsl:text>B.C.</xsl:text>
                     </p>
                     <p>
                        <xsl:text>B.C.E.</xsl:text>
                     </p>
                     <p>
                        <xsl:text>A.C.E.</xsl:text>
                     </p>
                  </div>
               </body>
            </html>
         </x:context>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc" as="document-node()">
               <xsl:document>
                  <html>
                     <head>
                        <meta charset="UTF-8"/>
                     </head>
                     <body>
                        <div class="docx-body">
                           <p>
                              <span class="tab">
                                 <xsl:comment> tab </xsl:comment>
                              </span>
                           </p>
                           <p>
                              <xsl:text>Paragraph above is just a tab</xsl:text>
                           </p>
                           <p>
                              <xsl:text>"'quote'"</xsl:text>
                           </p>
                           <p>
                              <xsl:text>'"quote"'</xsl:text>
                           </p>
                           <p>
                              <xsl:text>'&#x201d;quote"&#x2018;</xsl:text>
                           </p>
                           <p>
                              <xsl:text>&#x2018;"quote"&#x2019;</xsl:text>
                           </p>
                           <p>
                              <xsl:text>""quote""</xsl:text>
                           </p>
                           <p>
                              <xsl:text>&#x2014;&#x201d;</xsl:text>
                           </p>
                           <p>
                              <xsl:text>&#x201c;&#x2014;</xsl:text>
                           </p>
                           <p>
                              <xsl:text>&#x2019; quotation &#x2019;</xsl:text>
                           </p>
                           <p>
                              <xsl:comment> empty </xsl:comment>
                           </p>
                           <h2 style="font-family: Helvetica; font-weight: bold">
                              <xsl:text>These work:</xsl:text>
                           </h2>
                           <p>
                              <xsl:text>"straight quotes"</xsl:text>
                           </p>
                           <p>
                              <xsl:text>straight apostrophe'</xsl:text>
                           </p>
                           <p>
                              <xsl:text>don't</xsl:text>
                           </p>
                           <p>
                              <xsl:text>don&#x2018;t</xsl:text>
                           </p>
                           <p>
                              <xsl:text>don&#x2019;t</xsl:text>
                           </p>
                           <p>
                              <xsl:text>'simple'</xsl:text>
                           </p>
                           <p>
                              <xsl:text>`quote`</xsl:text>
                           </p>
                           <p>
                              <xsl:text>``quote``</xsl:text>
                           </p>
                           <p>
                              <xsl:text>&#x2018;qoutation&#x2018;</xsl:text>
                           </p>
                           <p>
                              <xsl:text>&#x201c;quotation&#x201c;</xsl:text>
                           </p>
                           <p>
                              <xsl:text>&#x2019;quotation&#x2019;</xsl:text>
                           </p>
                           <p>
                              <xsl:text>&#x201d;quotation&#x201d;</xsl:text>
                           </p>
                           <p>
                              <xsl:text>&#x201d;quotation``</xsl:text>
                           </p>
                           <p>
                              <xsl:text>``quotation&#x201d;</xsl:text>
                           </p>
                           <p>
                              <xsl:comment> empty </xsl:comment>
                           </p>
                           <p>
                              <xsl:text>sentence with punctuations separated by space .</xsl:text>
                           </p>
                           <p>
                              <xsl:text>sentence with punctuations separated by space !</xsl:text>
                           </p>
                           <p>
                              <xsl:text>sentence with punctuations separated by space ,</xsl:text>
                           </p>
                           <p>
                              <xsl:text>sentence with punctuations separated by space ,</xsl:text>
                           </p>
                           <p>
                              <xsl:text>sentence with punctuations separated by space ;</xsl:text>
                           </p>
                           <p>
                              <xsl:text>sentence with punctuations separated by space ?</xsl:text>
                           </p>
                           <p>
                              <xsl:text>sentence with punctuations separated by 3 spaces   ?</xsl:text>
                           </p>
                           <p>
                              <xsl:comment> empty </xsl:comment>
                           </p>
                           <p>
                              <xsl:comment> empty </xsl:comment>
                           </p>
                           <p>
                              <b>
                                 <xsl:text>this is all bold except for the period</xsl:text>
                              </b>
                              <xsl:text>.</xsl:text>
                           </p>
                           <p>
                              <i>
                                 <xsl:text>this is all italics except for the period</xsl:text>
                              </i>
                              <xsl:text>.</xsl:text>
                           </p>
                           <p>
                              <span style="font-family: Helvetica">
                                 <u>
                                    <xsl:text>this is all underlined</xsl:text>
                                 </u>
                              </span>
                              <a class="bookmarkStart" id="docx-bookmark_0">
                                 <xsl:comment> bookmark ='_GoBack'</xsl:comment>
                              </a>
                              <a href="#docx-bookmark_0">
                                 <xsl:comment> bookmark end </xsl:comment>
                              </a>
                              <span style="font-family: Helvetica">
                                 <u>
                                    <xsl:text> except for the period</xsl:text>
                                 </u>
                                 <xsl:text>.</xsl:text>
                              </span>
                           </p>
                           <p>
                              <xsl:comment> empty </xsl:comment>
                           </p>
                           <p>
                              <xsl:text>Summer of '69</xsl:text>
                           </p>
                           <p>
                              <xsl:text>Summer of &#x2018;69</xsl:text>
                           </p>
                           <p>
                              <xsl:comment> empty </xsl:comment>
                           </p>
                           <p>
                              <xsl:text>W.E.B. Dubois</xsl:text>
                           </p>
                           <p>
                              <xsl:text>E. B. White</xsl:text>
                           </p>
                           <p>
                              <xsl:comment> empty </xsl:comment>
                           </p>
                           <p>
                              <xsl:text>U.S.</xsl:text>
                           </p>
                           <p>
                              <xsl:text>D.C.</xsl:text>
                           </p>
                           <p>
                              <xsl:text>A.M.</xsl:text>
                           </p>
                           <p>
                              <xsl:text>P.M.</xsl:text>
                           </p>
                           <p>
                              <xsl:text>A.D.</xsl:text>
                           </p>
                           <p>
                              <xsl:text>B.C.</xsl:text>
                           </p>
                           <p>
                              <xsl:text>B.C.E.</xsl:text>
                           </p>
                           <p>
                              <xsl:text>A.C.E.</xsl:text>
                           </p>
                        </div>
                     </body>
                  </html>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="impl:context" select="$impl:context-doc/node()"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d7e153">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d7e153">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>Grab Bag of Substitutions</xsl:message>
      <xsl:variable name="impl:expected" select="()"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>Grab Bag of Substitutions</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
</xsl:stylesheet>
